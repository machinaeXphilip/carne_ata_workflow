#!/usr/bin/env python
# -*- coding: utf-8 -*-


# What this script does:
# get data form ALLTHINGI machina eX table (gotta be downloaded as excel file)
# and builds a new sheet inside of the excel with formatting like needed for 
# customs standart form "Carne ATA"
#
# how to use:
# 1) download your table, 
# 2) specify the col that is key (meaning: the col that 
# 	contains the unique ID of each item or at least is filled with data in every 
#	given row) by changing the variable "colUnique" (below)
# 3) specify (if needed) the location of the other colums inside of the for loop
# 	by changing the "column"-parameter to the number of the right col
# 4) run the script and enjoy! 


import openpyxl

wb = openpyxl.load_workbook("Allthingi.xlsx")
sheet = wb.get_sheet_by_name("Form responses 1")
sheetWrite = wb.get_sheet_by_name("Sheet1")

listOfRows = []
colUnique = 8

def searchCol(searchTerm):
	global listOfRows

	for cellObj in sheet.columns[2]:
		if cellObj.value != None:
			#print (cellObj.value)

			if cellObj.value == searchTerm:
				listOfRows.append(cellObj.row)
				# now the elegant part: (hurray 4 recursion!)
				# filter for things with Location inside of already found items: (e.g.: an item inside a box that has the location "LoL")
				searchCol(sheet.cell(row=cellObj.row, column=colUnique).value)


######################
####### MAIN #########
######################

# filter for things with Location "LoL"
searchCol("LoL")

# FILL RELEVANT DATA FROM ROWS INTO LIST AND THEN INTO NEW SHEET, ROW BY ROW:

newTableData = []

for count in range(len(listOfRows)):
	
	number = count + 1
	name = sheet.cell(row=listOfRows[count], column=2).value#.encode('utf-8')
	price = sheet.cell(row=listOfRows[count], column=5).value
	weight = sheet.cell(row=listOfRows[count], column=6).value
	itemsCount = sheet.cell(row=listOfRows[count], column=11).value
	
	newTableData.append([number, name, itemsCount, weight, price])
#print len(newTableData)

for count in range(len(newTableData)):

	#WRITE INTO FILE
	sheetWrite["A"+str(count+1)] = newTableData[count][0]
	sheetWrite["B"+str(count+1)] = newTableData[count][1]
	sheetWrite["C"+str(count+1)] = newTableData[count][2]
	sheetWrite["D"+str(count+1)] = newTableData[count][3]
	sheetWrite["E"+str(count+1)] = newTableData[count][4]

#print newTableData
wb.save('Allthingi.xlsx')