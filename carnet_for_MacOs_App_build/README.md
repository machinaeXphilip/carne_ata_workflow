# README #

* some python scripts to tackle the international customs form "Carne ATA".

### How do I get set up? ###

* install all dependencies (look into the .py scripts)
* download all base .pdf files
* put your excel file into the same directory
* run the scripts with python and enjoy.

if this saved you money and you can afford it, consider donating http://www.philipsteimel.de/donate/
