#!/usr/bin/python
# -*- coding: utf-8 -*-

# (CC BY-SA 2.5) by Philip Steimel 
# https://creativecommons.org/licenses/by-sa/2.5/
# 
# www.philipsteimel.de www.machinaex.de
# if you saved money with this tool and can afford it, consider a donation at:
# www.philipsteimel.de/donate/
# 
# THIS VERSION IS THE GUI VERSION FOR Mac OS !!!
# build with tkinter GUI


try:
	from tkinter import *
	print("using python3")
except:
	from Tkinter import *
	print("using python2")

import re
import openpyxl
import PyPDF2
from reportlab import rl_settings
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

import os.path


listOfNums =   []; listOfNames =  []; listOfItems =  []; listOfWeights = []; listOfPrices = []; listOfOrigins = []
lastItemNum = 0; totalWeight = 0; totalValue = 0

pageCount = 0

currency = "€"
unitOfWeight = "kg"
tableHasCountryOfOrigin = False# should be build in via user prompt
defaultOrigin = ""

##### GET TABLE DATA AND FILL IT INTO LISTS ######
"""

wb = openpyxl.load_workbook("carnet.xlsx")
sheet = wb.active
startrowShift = 1
#"""
"""
fileIn = raw_input("Enter filename of excel table or Return >>>")
if fileIn == "": 
    wb = openpyxl.load_workbook("carnet.xlsx")
else:
    wb = openpyxl.load_workbook(fileIn)
sheetIn = raw_input("Enter name of the table's sheet or Return >>>")
if sheetIn == "":
    sheet = wb.active
else:
    sheet = wb.get_sheet_by_name(sheetIn)
print
print ("If your table's data starts not in row 2 please")    
startrowShift = raw_input("Enter starting row or Return >>>")
if startrowShift == "" or startrowShift <= 1: 
    startrowShift = 1 
else: startrowShift = startrowShift - 1
print
print ("Does your script have a column with the country of origin?")
print ("Or should we add a default country of Origin if none is given?")
if raw_input("type y ( default is n ) >>") == "y":
    tableHasCountryOfOrigin = True
if tableHasCountryOfOrigin:
    defaultOrigin = raw_input("Enter default country of Origin >>>")
#"""

def show_entry_fields():
	global wb; global sheet; global tableHasCountryOfOrigin; global var; global defaultOrigin; global startrowShift

	tableHasCountryOfOrigin = bool(var.get()) # gets value of checkbox and fills it into var as bool

	print("Input File: %s\nSheets Name: %s" % (e1.get(), e2.get()))

	currency = "€"
	unitOfWeight = "kg"
	defaultOrigin = e3.get()
	print (defaultOrigin)

	##### GET TABLE DATA AND FILL IT INTO LISTS ######
	#"""
	if str(e1.get()) != "":
		wb = openpyxl.load_workbook(str(e1.get()))
	sheet = wb.active
	startrowShift = int(e4.get()) - 1
	doit()
	quit()


def doit():

	global listOfNums; global listOfNames;global listOfItems;global listOfWeights; global listOfPrices;global listOfOrigins
	global lastItemNum;global totalWeight ;global totalValue

	global currency; global unitOfWeight; global tableHasCountryOfOrigin; global defaultOrigin

	global wb
	global sheet
	global startrowShift


	for count in range(len(sheet.columns[2])-startrowShift):
	    listOfNums.append(str(sheet.cell(row=count+1+startrowShift, column=1).value))
	    listOfNames.append(sheet.cell(row=count+1+startrowShift, column=2).value)
	    listOfItems.append(str(sheet.cell(row=count+1+startrowShift, column=3).value))
	    listOfPrices.append(str(sheet.cell(row=count+1+startrowShift, column=4).value))
	    listOfWeights.append(str(sheet.cell(row=count+1+startrowShift, column=5).value))
	    if tableHasCountryOfOrigin:
	        listOfOrigins.append(sheet.cell(row=count+1+startrowShift, column=6).value)
	        if listOfOrigins[-1] is None:
	            listOfOrigins[-1] = defaultOrigin
	##################################################

	if len(listOfItems) == len(listOfNames) == len(listOfNums) == len(listOfWeights) == len(listOfPrices):
	    print ("data seems OK. starting...")
	else:
	    print ("ERROR: detected empty cells or flawed data. pls doublecheck.")
	    #print ("len of #itemnums: "+str(len(listOfNums))+" #descriptions: "+str(len(listOfNames))+" #num of items: "+str(len(listOfItems))+" #weights: "+str(len(listOfWeights))+" #value: "+str(len(listOfPrices)))
	    exit()

	itemCount = -len(listOfNums)

	def cycleThroughForm(itemCount, xshift = 0, yshift = 0, rownumshift = 0, filename = "carnet_1.pdf"):
	    pdfForm = open(filename, "rb")
	    destinationpath = str((os.path.abspath(os.pardir))).replace("/CarneATA.app/Contents","")+"/"
	    pdfReader = PyPDF2.PdfFileReader(pdfForm)
	    pdfBasis = pdfReader.getPage(0)

	    global pageCount; global lastItemNum; global totalWeight; global totalValue
	    pageCount += 1
	    
	    pdf = canvas.Canvas(destinationpath+'printform'+str(pageCount)+'.pdf',pagesize=A4) #Anlegen des PDF Dokuments, Seitengröße DIN A4 Hochformat)
	    pdf.setFont('Helvetica',10)
	    # if not first form, print sums of cols from last page into header of recent form page:
	    if pageCount > 1:
	        pdf.drawString(260+xshift,(660+yshift), lastItemNum)
	        pdf.drawString(305+xshift,(660+yshift), str(totalWeight) + unitOfWeight)
	        pdf.drawString(355+xshift,(660+yshift), str(totalValue) + currency)

	    linecount = 0
	    for count in range(47+rownumshift):
	        if linecount < 47+rownumshift:
	            if itemCount < 0:

	                    pdf.drawString(17+xshift,(640+yshift-linecount*10), listOfNums[itemCount])
	                    
	                    if listOfItems[itemCount] == "0" or not listOfItems[itemCount]:
	                        print ("found a zero in row #"+str(-itemCount)+" & changed it to '1'") 
	                        numOfItems = "1" # correct num of items if no number is provided
	                    else:
	                        numOfItems = listOfNums[itemCount]
	                    lastItemNum = listOfNums[itemCount]
	                    
	                    if len(listOfNames[itemCount]) > 34:
	                        part1 = listOfNames[itemCount][:34]; part2 = listOfNames[itemCount][34:] #this could be a loop i am sure! would be good for unlimited lines per item
	                        pdf.drawString(65+xshift,(640+yshift-linecount*10), part1)
	                        pdf.drawString(65+xshift,(640+yshift-linecount*10-10), part2)
	                        linecount += 1 # add a new line from now on.
	                    else:
	                        pdf.drawString(65+xshift,(640+yshift-linecount*10), listOfNames[itemCount])
	                    
	                    pdf.drawString(260+xshift,(640+yshift-linecount*10), listOfItems[itemCount])
	                    
	                    newWeight = cutStringsAndReturnFloat(listOfWeights[itemCount])
	                    #calc sum of weights so far:
	                    totalWeight += float(newWeight)
	                    pdf.drawString(305+xshift,(640+yshift-linecount*10), str(listOfWeights[itemCount]) + unitOfWeight)

	                    newValue = cutStringsAndReturnFloat(listOfPrices[itemCount])
	                    #calc sum of weights so far:
	                    totalValue += float(newValue)
	                    pdf.drawString(355+xshift,(640+yshift-linecount*10), str(listOfPrices[itemCount])+currency)
	                    if tableHasCountryOfOrigin:
	                        pdf.drawString(425+xshift,(640+yshift-linecount*10), str(listOfOrigins[itemCount]))

	                    linecount += 1
	                    itemCount += 1

	    # calculate sums of number of items, weight and price
	    lastItemNumOnPage = str(lastItemNum)
	    multipleRegex = re.compile(r'(.*)(-)*(\d)*')
	    lastItemNumOnPage = multipleRegex.search(lastItemNumOnPage)
	    lastItemNum = lastItemNumOnPage.group().split("-")[-1] # now we know the last items number on this page

	    #print the sums into the fields in the footer, checking beforehand if page no is odd or even
	    if pageCount == 1:
	        pdf.drawString(260+xshift,(130+yshift), lastItemNum)
	        pdf.drawString(305+xshift,(130+yshift), str(totalWeight)[:5] + unitOfWeight) #print first 5 chars
	        pdf.drawString(355+xshift,(130+yshift), str(totalValue) + currency)
	    elif pageCount % 2 == 0:
	        pdf.drawString(260+xshift,(160+yshift), lastItemNum)
	        pdf.drawString(305+xshift,(160+yshift), str(totalWeight)[:5] + unitOfWeight)
	        pdf.drawString(355+xshift,(160+yshift), str(totalValue) + currency)
	    else:
	        pdf.drawString(260+xshift,(100+yshift), lastItemNum)
	        pdf.drawString(305+xshift,(100+yshift), str(totalWeight)[:5] + unitOfWeight)
	        pdf.drawString(355+xshift,(100+yshift), str(totalValue) + currency)

	    # page done
	    pdf.showPage()
	    pdf.save()

	    watermarkFile = open(destinationpath+"printform"+str(pageCount)+".pdf", "rb")
	    watermarkRead = PyPDF2.PdfFileReader(watermarkFile)
	    watermark = watermarkRead.getPage(0)
	    pdfBasis.mergePage(watermark)
	    pdfWriter = PyPDF2.PdfFileWriter()
	    pdfWriter.addPage(pdfBasis)
	    resultPdfFile = open(destinationpath+"Form"+str(pageCount)+".pdf", "wb")
	    pdfWriter.write(resultPdfFile)

	    pdfForm.close()
	    resultPdfFile.close()
	    watermarkFile.close()

	    return itemCount

	    # here add drawString to fill out the Zwischensummen 
	    #   on the bottom of the form (and the top of the following)


	def cutStringsAndReturnFloat(inputString):
	    inputString = str(inputString)
	    inputString = inputString.replace("kg","")
	    inputString = inputString.replace("KG","")
	    inputString = inputString.replace("EUR","")
	    inputString = inputString.replace("eur","")
	    inputString = inputString.replace("€","")
	    inputString = inputString.replace(",",".")
	    return float(inputString)

	###################
	##### MAIN ########
	#print (itemCount)
	itemCount = cycleThroughForm(itemCount, 3,0,1, "carnet_1.pdf") # Form 1 (Warenliste Hauptantrag) is only used for the very first page
	#print (itemCount)

	# Form 2 and Form 3 are used constantly
	while itemCount < 0: 
	    itemCount = cycleThroughForm(itemCount, 50, -33, -1, "carnet_2.pdf") # Form 2 (Anhang Vorderseite)
	    if itemCount < 0:
	        itemCount = cycleThroughForm(itemCount ,0, -33, 5, "carnet_3.pdf") # Form 3 (Anhang Rückseite)

	print("finished all items.")
	exit()





###### MAIN ######
##################

# figure out the parent path of .app (we are two dirs deeper)
parentpath = str((os.path.abspath(os.pardir))).replace("/CarneATA.app/Contents","")+"/"
#parentpath = str((os.path.abspath(os.pardir)))+"/" # IF STARTED AS SCRIPT AND NOT AS .APP
print (parentpath)

master = Tk()
master.wm_title("Carne A.T.A PDF Filler")

Label(master, text="This App will pull data out of an xls or xlsx file and produce\n pdfs from it that can be printed into the standart Carnet A.T.A. forms \n Please provide a file with path \n\n The PDF files will be found in the directory where this app is located.\n"+"_"*70).grid(columnspan=2, pady=10)

Label(master, text="Input File").grid(row=1)
Label(master, text="Sheet Name \n (If not 1st)").grid(row=2)
Label(master, text="Default Country\n of Origin (If any)").grid(row=4)
Label(master, text="Number of first\n row with data \n (in case you have\n a header row)").grid(row=6)
Label(master, text="    ").grid(column=4, row=5)

var = IntVar()
c = Checkbutton(master, text="Column 'country of origin' in table?", variable=var)

e1 = Entry(master,width=30)
e1.insert(0,parentpath+"carnet.xlsx")
e2 = Entry(master,width=30)
e2.insert(0,"Sheet 1")
e3 = Entry(master,width=30)
e4 = Entry(master,width=30)
e4.insert(0,"2")

e1.grid(row=1, column=1)
e2.grid(row=2, column=1)
e3.grid(row=4, column=1)
e4.grid(row=6, column=1)
c.grid(row=3, column=1, sticky=W, padx=10)

Button(master, text='Quit', command=master.quit).grid(row=8, column=1, sticky=W, pady=4, padx=100)
Button(master, text='Print PDFs', command=show_entry_fields).grid(row=8, column=1, sticky=W, pady=4)

Label(master, text="_"*75+"\n(cc-by-sa)\n Philip Steimel / machina ESC\n"+"_"*75+"\nwww.philipsteimel.de/donate").grid(columnspan=9)

mainloop( )

