# README #

* some python scripts to tackle the international customs form "Carne ATA".

### How do I get set up? ###

* on MacOS: Go to Downloads and dowload the Repository or only  CarneAppMacOs.zip
	* they contain: an MacOs executable .app and an example .xlxs table
	* the .app should work right out of the box
	* you can use the care.xlxs example file to check what the programs outputs are and how to format your own table (if necessary)

* if you are on linux or windows:
	* make sure you have python installed (if windows)
	* install all dependencies (look into the .py scripts --> install all after "import" keyword)
	* download all base .pdf files (carne_1.pdf and so on)
	* put your excel file into the same directory
	* run the scripts with python and enjoy.

if this saved you money and you can afford it, consider donating http://www.philipsteimel.de/donate/


### PLANNED FEATURES: ###

* more flexible excel reading capabilities
	* change the column numbers that are used to fill the carne ata forms manually (in case your table has different formatting)

